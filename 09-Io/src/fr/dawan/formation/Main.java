package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

    public static void main(String[] args) {

        System.out.println(File.separator); // File.séparator Le caractère de séparation par défaut dépendant du système

        ecrireText("C:\\Formations\\TestIO\\test1.txt");
        ecrireTextBuff("C:\\Formations\\TestIO\\test2.txt");
        ecrireTextPrint("C:\\Formations\\TestIO\\test3.txt");
        lireText("C:\\Formations\\TestIO\\test1.txt");
        lireTextBuff("C:\\Formations\\TestIO\\test2.txt");

        // Exercice : Copie de fichier
        copier("C:\\Formations\\TestIO\\PORCO ROSSO.jpg", "C:\\Formations\\TestIO\\copie.jpg");

        // Parcourir le répertoire du projet 03-poo
        // File : représentation des chemins d'accès aux fichiers et aux répertoires
        File f = new File("C:\\Formations\\Java\\Workspace\\03-Poo");
        parcourir(f);
    }

    // Ecrire dans un fichier texte (sans try with ressource)
    public static void ecrireText(String path) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(path, true); // true : on ajoute des données au fichier, false : le fichier est écrasé
                                             // (par défaut)
            for (int i = 0; i < 10; i++) {
                fw.write("Hello World"); // écriture d'une chaine dans le fichier
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close(); // fermeture du flux
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Utilisation de try with ressource pour fermer automtiquement le flux vers le fichier (à partir du java 7)
    // try with ressource fonctionne avec tous les objets implémentant l'interface AutoCloseable
    public static void ecrireTextBuff(String path) {
        // On utilise un BufferedWriter, il n'a pas accès directement au fichier, il faut passer par un FileWriter
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            for (int i = 0; i < 10; i++) {
                bw.write("Hello World");
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Ecrire dans un fichier texte avec PrintWriter
    public static void ecrireTextPrint(String path) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(path))) {
            for (int i = 0; i < 10; i++) {
                pw.println(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Lecture d'un fichier texte
    public static void lireText(String path) {
        char[] buff = new char[10];
        try (FileReader fr = new FileReader(path)) {
            while (fr.read(buff) > 0) { // lecture de 10 caractères maximum dans le fichier
                for (char c : buff) { // lorsque l'on atteint la fin du fichier read retourne -1
                    System.out.print(c);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lecture d'un fichier texte avec BufferedReader
    public static void lireTextBuff(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            while (true) {
                String l = br.readLine();
                if (l == null) {
                    break;
                }
                System.out.println(l);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Exercice: faire une méthode qui va copier un fichier octet par octet
    public static void copier(String pathSrc, String pathTrg) {
        try (FileInputStream fi = new FileInputStream(pathSrc); FileOutputStream fo = new FileOutputStream(pathTrg)) {

            while (true) {
                int b = fi.read();
                if (b == -1) {
                    break;
                }
                fo.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Parcourir un système de fichiers
    public static void parcourir(File f) {
        if (f.exists()) {   // Test l'existence du fichier ou du dossier
            if (f.isDirectory()) {  // si c'est un dossier
                System.out.println("Répertoire=" + f.getName());     // affichage du nom du répertoire
                System.out.println("_____________________");
                File[] tabF = f.listFiles();    // Récupération du contenu du dossier
                for (File fi : tabF) {
                    parcourir(fi);  // Appel récursif sur chaque fichier du dossier
                }
            } else {
                System.out.println(f.getName());    // affichage du nom du fichier
            }
        } else {
            f.mkdir();
        }
    }

}
