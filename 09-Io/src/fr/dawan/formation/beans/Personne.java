package fr.dawan.formation.beans;
import java.io.Serializable;

// Pour qu'une classe soit sérialisable, elle doit implémenter l'interface Serializable
public class Personne implements Serializable{
    
 // serialVersionUID est une clé de hachage SHA qui identifie de manière unique la classe.
    // Si la classe personne évolue et n'est plus compatible avec les objets précédamment persistés, on modifie la valeur du serialVersionUID
    // et lors de la désérialisation une exception sera générée pour signaler l'incompatibilité
    // java.io.InvalidClassException: fr.dawan.formation.beans.Personne; local class incompatible: stream classdesc serialVersionUID = 1, local class serialVersionUID = 2
    // si l'on fournit pas serialVersionUID le compilateur va en générer un (à éviter).
    private static final long serialVersionUID = 1L;
    
    private String prenom;
    private String nom;
    private Adresse adresse;
    private transient String password;  // Si un attribut ne doit pas être sérialiser on ajout le mot clef transient
    
    private static int cpt; // les variable de classe ne sont pas sérialisée
    
    public Personne() {
    }

    public Personne(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }
    
    public Personne(String prenom, String nom, Adresse adresse) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Personne [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + "]";
    }

}