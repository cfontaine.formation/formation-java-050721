package fr.dawan.formation;
public class Main {

    public static void main(String[] args) {
       Box<String> strBox=new Box<>();
       strBox.setT("Hello");
       System.out.println(strBox);
       
       Box<Integer> intBox=new Box(123);
       System.out.println(intBox);
       
       System.out.println(Calcul.<Double>egal(4.5,5.6) );
       System.out.println(Calcul.egal(4.5,4.5) );
       System.out.println(Calcul.egal("Hello","Bonjour") );
    }

}
