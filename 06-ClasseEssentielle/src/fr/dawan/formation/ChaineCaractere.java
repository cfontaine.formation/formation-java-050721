package fr.dawan.formation;
import java.nio.file.FileSystemLoopException;
import java.util.StringTokenizer;

public class ChaineCaractere {

    public static void main(String[] args) {
        String s1 = "test"; // Une chaine litérale est stockée dans le stringpool
        String s2 = "test"; // Il n'y a qu'une seule objet String qui contient test
        System.out.println((s1 == s2) + " - " + s1.equals(s2));
        // s1==s2 true, s1 et s2 pointe vers un seul objet
        // s1.equals(s2) true equals compare 2 chaines de caractère
        String s3 = new String("test"); // une chaine de caractère créé avec le constructeur est stocké dans le heap
        String s4 = new String("test"); // Il y a 2 objets créés en mémoire
        System.out.println((s3 == s4) + " - " + s3.equals(s4));
        // s3==s4 false s3 et s4 ne sont pas égal, il pointe sur 2 objets différents
        // s3.equals(s4) true equals compare 2 chaines de caractère
        String str = "Hello World";
        String str2 = "Bonjour";
        String str3 = "Hello World";

        // length -> Nombre de caractère de la chaine de caractère
        System.out.println(str.length());

        // Comparaison 0-> égale, >0 -> se trouve après, <0 -> se trouve avant dans l'ordre alphabétique
        // valeur retournée: distance entre les 2 chaine B -> H =6
        System.out.println(str.compareTo(str2)); // >0
        System.out.println(str2.compareTo(str)); // <0
        System.out.println(str.compareTo(str3)); // ==0

        // Concaténation
        System.out.println(str + str2); // + -> concaténation
        System.out.println(str.concat(str2));

        // La méthode join concatène les chaines, en les séparants par une chaine de séparation
        String strJ = String.join(";", "azerty", "uiop", "sdfhj");
        System.out.println(strJ); // "azerty;uiop;sdfhj"

        // Découpe la chaine et retourne un tableau de sous-chaine suivant un séparateur
        String[] tabStr = strJ.split(";");
        for (String s : tabStr) {
            System.out.println(s);
        }

        // substring permet d'extraire une sous-chaine
        // de l'indice passé en paramètre jusqu'à la fin de la chaine
        System.out.println(str.substring(6)); // World
        // de l'indice passé en paramètre jusqu'à l'indice de fin (exclut)
        System.out.println(str.substring(6, 9)); // Wor

        // startsWith retourne true, si la chaine commence par la chaine passé en
        // paramètre
        System.out.println(str.startsWith("Hello")); // true
        System.out.println(str.startsWith("Bonjour")); // false

        // indexOf retourne la première occurence de la chaine passée en paramètre
        System.out.println(str.indexOf("o")); // 4
        System.out.println(str.indexOf("o", 5)); // idem mais à partir de l'indice passé en paramètre 7
        System.out.println(str.indexOf("o", 8)); // retourne -1, si le caractère n'est pas trouvé

        // Remplace toutes les les sous-chaines target par replacement
        System.out.println(str.replace('o', '_')); // Hell_ W_rld

        // Retourne true, si la sous-chaine passée en paramètre est contenu dans la  chaine
        System.out.println(str.contains("Wo"));// true
        System.out.println(str.contains("aa")); // false

        // Retourne le caractère à l'indice 4
        System.out.println(str.charAt(2)); // l

        // Retourne la chaine en majuscule
        System.out.println(str.toUpperCase());
        System.out.println("éeè".toUpperCase());

        // trim supprime les caractères de blanc du début et de la fin de la chaine
        System.out.println("   \n \t \t azerty uiop \n \t  \n\n  ".trim());

        // Retourne une chaine formater
        System.out.println(String.format("%d -- [%s]", 10, "Bonjour"));

        // les chaines de caractères sont immuables une fois créée elles ne peuvent plus être modifiées
        str.toLowerCase();
        str.substring(4);
        str.trim();
        System.out.println(str);

        //
        System.out.println(str.toLowerCase().substring(4).toUpperCase());

        // StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation
        // et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un
        // objet String => pas de création d'objet intermédiare
        StringBuilder sb = new StringBuilder("Hello");
        sb.append(4.0);
        sb.append("World");
        sb.insert(2, "______________");
        String str4 = sb.toString();
        System.out.println(str4);

        // StringTokenizer
        // Permet de décomposer une chaîne de caractères en une suite de mots séparés
        // par des délimiteurs
        StringTokenizer tok = new StringTokenizer(strJ, ";");
        System.out.println(tok.countTokens());
        while (tok.hasMoreElements()) {
            System.out.println(tok.nextToken());
        }

        // Excercice Chaine de caractère
        System.out.println(inverser("Bonjour"));
        System.out.println(palindrome("Radar"));
        System.out.println(palindrome("SOS"));
        System.out.println(palindrome("Bonjour"));
    }

    static String inverser(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }

    static boolean palindrome(String str) {
        String tmp = str.trim().toLowerCase();
        return tmp.equals(inverser(tmp));
    }

}
