package fr.dawan.formation;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;

public class Temps {

    public static void main(String[] args) {
        Date d=new Date();
        System.out.println(d);
        
        LocalDate today=LocalDate.now();
        System.out.println(today);
        LocalDate noel2021=LocalDate.of(2021, Month.DECEMBER, 25); // LocalDate.of(2021, 12, 25);
        System.out.println(noel2021);
        
        LocalTime currentTime=LocalTime.now();
        System.out.println(currentTime);
        
        LocalDateTime ldt=LocalDateTime.now();
        System.out.println(ldt);
        
        LocalDate vacance=today.plusWeeks(3).plusDays(1);
        System.out.println(vacance);
        
        Period per=Period.ofYears(1).ofMonths(3).ofDays(10);
        System.out.println(per); // 10 jours
        
        Period per2=Period.between(today, noel2021);
        System.out.println(per2);
        
        System.out.println(today.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(today.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        System.out.println(today.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        System.out.println(today.format(DateTimeFormatter.ofPattern("dd/MM/YYYY")));
    }

}
