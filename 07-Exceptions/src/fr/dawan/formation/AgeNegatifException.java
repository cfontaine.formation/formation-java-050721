package fr.dawan.formation;

public class AgeNegatifException extends Exception {

    private static final long serialVersionUID = 1L;
    
    public AgeNegatifException(int age) {
        super("L'age est négatif: "+age);
    }

}
