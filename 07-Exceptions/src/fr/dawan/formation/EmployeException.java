package fr.dawan.formation;

public class EmployeException extends Exception {

    private static final long serialVersionUID = 1L;

    public EmployeException(String msg,AgeNegatifException e) {
        super(msg,e);
    }
}
