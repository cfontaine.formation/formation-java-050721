package fr.dawan.formation;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main {


    @SuppressWarnings({ "resource", "unused" })
    public static void main(String[] args) {
        
        System.out.println("Début Programme");
        // checked exception => on est obligé (par java) de traiter l'exception
        try {
            FileInputStream fo=new FileInputStream("nexistepas"); // // ouverture d'un fichier qui n'existe pas => lancer une exception FileNotFoundException
            int a=fo.read();
            System.out.println("Suite Programme");
        }

        catch(FileNotFoundException e) {    // si une exception FileNotFoundException ce produit dans le bloc try
            System.out.println("Le fichier n'existe pas");
        }                                   // pour un bloc try, il peut y avoir plusieurs blocs catch pour traiter diférentes exceptions
                                            // de la - à la + générale
        catch(IOException | NullPointerException e) {  // Un
            System.err.println("Lecture impossible");
        }
        catch(Exception e) {
            System.err.println("Un autre exception");
        }
        finally {                           // le bloc finally est toujours exécuté
            System.err.println("Libérer les ressources");
        }
        
        // Runtime Exception => on peut traiter l'exception ou pas (pas d'obligation)
        try {
            double[] tab = new double[10];
            tab[100] = 56.78;
        } catch (ArrayIndexOutOfBoundsException e) {
            // e.printStackTrace();
            System.err.println("Indice en dehors du tableau " + e.getMessage());
        }
        
        Scanner sc=new Scanner(System.in);
        int age=sc.nextInt();
        try {
            TraitementEmploye(age);
        } catch (EmployeException e) {
            System.out.println("Traitement main:" + e.getMessage());
        }
        
        sc.close();
        System.out.println("Fin Programme");
    }
    
    public static void TraitementEmploye(int age) throws EmployeException {
        System.out.println("Traitement Employe");
        try {
            traitementAge(age);
        } catch (AgeNegatifException e) {
            System.out.println("Traitement partiel de l'exception");
           // throw e;
            throw new EmployeException("erreur traitement employé",e);
        }
        System.out.println("Fin Traitement Employe");
    }
    
    public static void traitementAge(int age) throws AgeNegatifException{
        System.out.println("Traitement Age");
        if (age<0) {
            throw new AgeNegatifException(age);//Exception("Age négatif :" + age );
        }
        System.out.println("Fin Traitement Age");
    }

}
