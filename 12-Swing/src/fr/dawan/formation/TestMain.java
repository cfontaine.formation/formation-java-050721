package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TestMain {

    public static void main(String[] args) {
       JFrame frame=new JFrame("Test Swing");
       frame.setSize(800,600);
       frame.setMinimumSize(new Dimension(300,200));
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
       //frame.setLayout(null);
      // frame.setLayout(new FlowLayout(FlowLayout.LEFT,50,50));
       frame.setLayout(new BorderLayout());
       JButton bp1=new JButton("Ok");
       bp1.setPreferredSize(new Dimension(200,100));
       // bp1.setBounds(100, 100, 200, 100);
       BoutonAction ba=new BoutonAction();
       bp1.addActionListener(ba);
       bp1.setActionCommand("action BP1");
//       bp1.addActionListener(new ActionListener() {
//        
//        @Override
//        public void actionPerformed(ActionEvent e) {
//            System.out.println("BP1");
//            
//        }
//        
//        
//    });
//       bp1.addMouseListener(new MouseAdapter() {
//
//        @Override
//        public void mouseEntered(MouseEvent e) {
//            System.out.println("Enter "+e.getX());
//        }
//
//        @Override
//        public void mouseMoved(MouseEvent e) {
//            System.out.println(e.getX() + e.getY());
//        }
//    });
       JButton bp2=new JButton("not Ok");
       bp2.setPreferredSize(new Dimension(200,100));
       bp2.addActionListener(ba);
       bp2.setActionCommand("action BP2");
       JPanel pan1=new JPanel();
       pan1.setLayout(new FlowLayout());
       pan1.add(bp1);
       pan1.add(bp2);
       frame.getContentPane().add(pan1,BorderLayout.SOUTH);
       frame.setVisible(true);
    }

}
