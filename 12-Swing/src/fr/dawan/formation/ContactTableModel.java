package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.dawan.formation.beans.Contact;

public class ContactTableModel extends AbstractTableModel {


    private static final long serialVersionUID = 1L;

    private List<Contact> contacts;
    
    public ContactTableModel(List<Contact> contacts) {
        super();
        this.contacts = contacts;
    }

    @Override
    public int getRowCount() {

        return contacts.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Contact dataRow=contacts.get(rowIndex);
        switch(columnIndex) {
        case 0:
            return dataRow.getPrenom();
        case 1:
            return dataRow.getNom();
        case 2:
            return dataRow.getDateNaissance();
        case 3:
            return dataRow.getEmail();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
        case 0:
            return "Prénom";
        case 1:
            return "Nom";
        case 2:
            return "Date de Naissance";
        case 3:
            return "Email";
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if(columnIndex==2) {
            return LocalDate.class;
        }
        else {
            return String.class;
        }
    }
    

}
