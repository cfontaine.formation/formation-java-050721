package fr.dawan.formation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

import fr.dawan.formation.dao.ContactDao2;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class Application {

    private JFrame frmApplication;
    private JTable table;


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application window = new Application();
                    window.frmApplication.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Application() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmApplication = new JFrame();
        frmApplication.setTitle("Application");
        frmApplication.setBounds(100, 100, 1013, 611);
        frmApplication.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel pan1 = new JPanel();
        frmApplication.getContentPane().add(pan1, BorderLayout.SOUTH);
        
        JButton btnOk = new JButton("OK");
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               int choix=JOptionPane.showConfirmDialog(frmApplication,"Test");
               System.out.println("Choix="+choix);

            }
        });
        pan1.add(btnOk);
        
        JButton btnCancel = new JButton("Annuler");
        pan1.add(btnCancel);
        
        JScrollPane scrollPane = new JScrollPane();
        frmApplication.getContentPane().add(scrollPane, BorderLayout.CENTER);
        
        
        
        table = new JTable();
        scrollPane.setViewportView(table); // Il faut utiliser un JScrollPane sinon on n'affiche pas le titre des colonnes
        ContactDao2 dao=new ContactDao2();
        table.setModel(new ContactTableModel(dao.findAll(true)));
    }

}
