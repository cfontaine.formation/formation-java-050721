package fr.dawan.poo.beans;

public class Moteur {
    
    int puissanceCh;
    boolean allumer;

    public Moteur(int puissanceCh) {
        this.puissanceCh = puissanceCh;
    }
    
    void demarer() {
        allumer=true;
    }
    void arreter() {
        allumer=false;
    }

}
