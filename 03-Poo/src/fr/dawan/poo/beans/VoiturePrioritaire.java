package fr.dawan.poo.beans;

public class VoiturePrioritaire extends Voiture {

    private boolean gyro;

    public VoiturePrioritaire() {
        super("Ford", "Blanc", "XX-XXXX-XXX");      // Appel explicite du constructeur 3 paramètres de la classe mère
        System.out.println("constructeur par défaut VoiturePrioritaire");
    }

    public VoiturePrioritaire(boolean gyro) {
        // implicitement le constructeur par défaut de la classe mère est appeler si l'on indique rien
        System.out.println("constructeur  VoiturePrioritaire");
        this.gyro = gyro;
    }

    public void allumerGyro() {
        gyro = true;
    }

    public void eteindreGyro() {
        gyro = false;
    }

    public boolean isGyro() {
        return gyro;
    }

    // Redéfinition de la méthode afficher
    @Override
    public void afficher() {
        super.afficher(); // appel de la méthode afficher de la classe mère
        System.out.println("gyro=" + gyro);
    }

    private void test() {
    }
}
