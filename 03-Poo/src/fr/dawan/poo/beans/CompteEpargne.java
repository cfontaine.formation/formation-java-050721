package fr.dawan.poo.beans;

public class CompteEpargne extends CompteBancaire {

    private double taux = 0.75;

    public CompteEpargne(double solde, Personne titulaire, double taux) {
        super(solde, titulaire);
        this.taux = taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    public void calculInterets() {
        solde *= (1 + taux / 100);
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("Taux= " + taux);
    }

}
