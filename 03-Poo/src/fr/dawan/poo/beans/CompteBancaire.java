package fr.dawan.poo.beans;

public class CompteBancaire {
    protected double solde = 50.0;
    private String iban;
    private Personne titulaire;

    private static int nbCompte;

    public CompteBancaire() {
        nbCompte++;
        iban = "fr5900-" + nbCompte;
    }

    public CompteBancaire(Personne titulaire) {
        this();
        this.titulaire = titulaire;
    }

    public CompteBancaire(double solde, Personne titulaire) {
        this(titulaire);
        this.solde = solde;
    }

    public void afficher() {
        System.out.println("________________");
        System.out.println("Solde=" + solde);
        System.out.println("Iban=" + iban);
        System.out.print("Titulaire=  ");
        if (titulaire != null) {
            titulaire.afficher();
        }
        System.out.println("________________");
    }

    public void crediter(double valeur) {
        if (valeur > 0.0) {
            solde += valeur;
        }
    }

    public void debiter(double valeur) {
        if (valeur > 0.0) {
            solde -= valeur;
        }
    }

    public boolean estPositif() {
        return solde > 0;
    }

    public Personne getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(Personne titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public String getIban() {
        return iban;
    }
}
