package fr.dawan.poo.beans;

public /* final */ class Voiture { // final => interdire l'héritage à partir de la classe

    public static final int VITESSE_MAXIMUM = 130;

    // Variable d'instance (attribut)

    // L'encapsulation consiste à cacher l'état interne d'un objet et d'imposer de
    // passer par des méthodes permettant un accès sécurisé à l'état de l'objet
    // private => attribut accessible seulement dans la classe elle-même
    private String marque;
    private String couleur = "Noir"; // Noir valeur par défaut de l'attribut couleur
    private String plaqueIma;
    private int vitesse;
    private int compteurKm = 10;// 10 valeur par défaut de l'attribut compteurKm
    // Agrégation
    private Personne proprietaire;
    // Composition
    private Moteur moteur;

    // Variable de classe
    private static int cptVoiture;

    // Constructeurs
    // Constructeur par défaut (sans paramètres)
    public Voiture() {
        System.out.println("Constructeur par défaut: Voiture");
        moteur = new Moteur(75);
        cptVoiture++;
    }

    // On peut surcharger le constructeur
    public Voiture(String marque, String couleur, String plaqueIma) {
        this(); // Appel d'un autre constructeur avec this, doit être la première instruction du
                // constructeur
        System.out.println("Constructeur 3 paramètres: Voiture");
        this.marque = marque; // utilisation de this pour indiquer que l'on acccède à la variable d'instance
        this.couleur = couleur;
        this.plaqueIma = plaqueIma;
    }

    public Voiture(String marque, String couleur, String plaqueIma, int vitesse, int compteurKm) {
//        this.marque = marque;
//        this.couleur = couleur;
//        this.plaqueIma = plaqueIma;
        this(marque, couleur, plaqueIma);
        this.vitesse = vitesse;
        this.compteurKm = compteurKm;
    }

    public Voiture(String marque, String couleur, String plaqueIma, Personne proprietaire) {
        this.marque = marque;
        this.couleur = couleur;
        this.plaqueIma = plaqueIma;
        this.proprietaire = proprietaire;
    }

    // Getters/Setters
    // Un getter permet l'accès en lecture à un attribut
    public String getCouleur() {
        return couleur;
    }

    // Un setter permet de demander un changement d'état
    public void setCouleur(String couleur) {
        if (couleur != null) {
            this.couleur = couleur;
        }
    }

    public String getPlaqueIma() {
        return plaqueIma;
    }

    public void setPlaqueIma(String plaqueIma) {
        this.plaqueIma = plaqueIma;
    }

    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

    public String getMarque() {
        return marque;
    }

    public int getVitesse() {
        return vitesse;
    }

    public int getCompteurKm() {
        return compteurKm;
    }

    public static int getCptVoiture() {
        return cptVoiture;
    }

    // Méthode d'instance
    public void accelerer(int vAcc) {
        if (vAcc >= 0) {
            vitesse += vAcc;
        }
    }

    public void freiner(int vFrn) {
        if (vFrn >= 0) {
            vitesse -= vFrn;
        }
    }

    public void arreter() {
        vitesse = 0;
        moteur.arreter();
    }

    public boolean estArreter() {
        return vitesse == 0;
    }

    public int getPuissance() {
        return moteur.puissanceCh;
    }

    public /* final */ void afficher() { // final => interdire la redéfinition d'une méthode
        System.out.println(marque + " " + couleur + " " + plaqueIma + " " + vitesse + " " + compteurKm);
    }

    // Méthode de classe
    public static void testMethodeClasse() {
        System.out.println("Méthode de classe");
        System.out.println(cptVoiture);
        // vitesse=0; // Erreur=> une méthode de classe ne peut pas accèder à une
        // variable d'instance
        // arreter(); // ou à une méthode d'instance
    }

    public static boolean egaliteVitesse(Voiture va, Voiture vb) {
        // Dans un méthode de classe on peut accèder à une variable d'instance
        // si la référence d'un objet est passée en paramètre
        return va.vitesse == vb.vitesse;
    }

    private void test() {
    }
}
