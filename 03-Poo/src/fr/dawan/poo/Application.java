package fr.dawan.poo;



import java.util.Date;
import java.sql.*;  // importe toutes les classes du package java.sql sauf Date

import fr.dawan.poo.beans.Adresse;
import fr.dawan.poo.beans.CompteBancaire;
import fr.dawan.poo.beans.CompteEpargne;
import fr.dawan.poo.beans.Personne;
import fr.dawan.poo.beans.Voiture;
import fr.dawan.poo.beans.VoiturePrioritaire;

// ou 
// import fr.dawan.poo.beans.*; // *-> importe toutes les classes du package

public class Application {

    public static void main(String[] args) {
        System.out.println("Nombre voiture=" + Voiture.getCptVoiture()); // Voiture.cptVoiture

        // Appel d'une méthode de classe
        Voiture.testMethodeClasse();

        // Instantiation de la classe Voiture
        Voiture v1 = new Voiture();
        // Pour modifier les attributs, on doit passer par les setters
        v1.setCouleur("Vert"); // v1.couleur="Vert" n'est plus accessible => privé
        System.out.println("Nombre voiture=" + Voiture.getCptVoiture());
        System.out.println(v1.getVitesse()); // v1.vitesse
        System.out.println(v1.getCompteurKm()); // v1.compteurKm
        System.out.println(v1.getCouleur()); // v1.couleur

        // Appel d’une méthode d’instance
        v1.accelerer(30);
        System.out.println(v1.getVitesse());
        v1.freiner(20);
        System.out.println(v1.getVitesse());
        System.out.println(v1.estArreter());
        v1.arreter();
        System.out.println(v1.estArreter());

        Voiture v2 = new Voiture();
        System.out.println("Nombre voiture= " + Voiture.getCptVoiture());
        // v2.vitesse = 20;

        System.out.println(v2.getVitesse());

        Voiture v3 = new Voiture("Honda", "Blanc", "Fr-1345-Az");
        System.out.println("Nombre voiture=" + Voiture.getCptVoiture());

        System.out.println(Voiture.egaliteVitesse(v2, v3));

        // v3=null;
        // System.gc(); // Appel explicite du garbage collector => à eviter

        Adresse adrLille = new Adresse("1, rue esquermoise", "Lille", "59800");
        Personne per1 = new Personne("John", "Doe", adrLille);
        Voiture v4 = new Voiture("Opel", "Gris", "fr5656-rt", per1);
        System.out.println(v4.getProprietaire().getNom());

        // Compte Bancaire
        CompteBancaire cb = new CompteBancaire(100.0, per1);

        // cb.solde=100.0;
        // cb.iban = "fr6259-0000-0000";
        // cb.titulaire="John Doe";

        cb.afficher();
        cb.crediter(50.0);
        cb.afficher();
        System.out.println(cb.estPositif());
        cb.debiter(200.0);
        cb.afficher();
        System.out.println(cb.estPositif());

        Personne per2 = new Personne("Jane", "Doe", new Adresse("32 Boulevard Vincent Gâche", "Nantes", "4400"));
        CompteBancaire cb2 = new CompteBancaire(400.0, per2);
        cb2.afficher();

        // Package
        Date d1 = new Date();
        java.sql.Date dSql = new java.sql.Date(0);
        ResultSet res = null;

        // Héritage
        VoiturePrioritaire vp1 = new VoiturePrioritaire(true);
        vp1.eteindreGyro();

        // Redéfinition de méthode
        VoiturePrioritaire vp2 = new VoiturePrioritaire();
        vp2.afficher();

        // Exercice: Compte épargne
        CompteEpargne ce1 = new CompteEpargne(200.0, per1, 5);
        ce1.calculInterets();
        ce1.afficher();
    }

}
