import java.util.Scanner;

public class Instructions {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // Condition if
        double d = sc.nextDouble();
        if (d > 10.0) {
            System.out.println("sup à 10");
        } else if (d == 10.0) {
            System.out.println("égal à 10");
        } else {
            System.out.println("inf à 10");
        }

        // Exercice: Trie de 2 valeurs
        // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la
        // forme 1.5 < 10.5
        System.out.println("Entrer 2 nombres");
        double d1 = sc.nextDouble();
        double d2 = sc.nextDouble();
        if (d1 < d2) {
            System.out.println(d1 + " < " + d2);
        } else {
            System.out.println(d2 + " < " + d1);
        }

        // Exercice: Intervalle
        // Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7
        // (inclus)
        System.out.println("Entrer un nombre entier");
        int v = sc.nextInt();
        if (v > -4 && v <= 7) {
            System.out.println(v + " fait partie de l'intervalle");
        }

        // Condition switch
        int jours = sc.nextInt();

        switch (jours) {
        case 1:
            System.out.println("Lundi");
            break;
        case 6:
        case 7:
            System.out.println("week end !");
            break;
        default:
            System.out.println("autre jour");
        }

        // Exercice Calculatrice
        // Faire un programme calculatrice
        // Saisir dans la console
        // - un double v1
        // - une chaine de caractère opérateur qui a pour valeur valide : + - * /
        // - un double v2
        // Afficher:
        // - Le résultat de l’opération
        // - Une message d’erreur si l’opérateur est incorrecte
        // - Une message d’erreur si l’on fait une division par 0
        double v1 = sc.nextDouble();
        String op = sc.next();
        double v2 = sc.nextDouble();
        switch (op) {
        case "+":
            System.out.println(v1 + " + " + v2 + " = " + (v1 + v2));
            break;
        case "-":
            System.out.println(v1 + " - " + v2 + " = " + (v1 - v2));
            break;
        case "*":
            System.out.println(v1 + " * " + v2 + " = " + (v1 * v2));
            break;
        case "/":
            if (v2 == 0.0) {
                System.out.println("Division par 0");
            } else {
                System.out.println(v1 + " / " + v2 + " = " + (v1 / v2));
            }
            break;
        default:
            System.out.println(op + " n'est pas un opérateur valide");
        }

        // Condition opérateur ternaire
        // Exercice parité
        // Créer un programme qui indique, si le nombre entier saisie dans la console
        // est paire ou impaire en utilisant un opérateur ternaire
        int p = sc.nextInt();
        String str = p % 2 == 0 ? "paire" : "impaire";
        System.out.println(p + " est " + str);

        // Boucle while / do while
        int j = 0;
        while (j < 10) {
            System.out.println(j);
            j++;
        }

        j = 0;
        do {
            System.out.println(j);
            j++;
        } while (j < 10);

        // Boucle for
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }

        //
        for (int i = 0, n = 2; i < 10; i++, n += 2) {
            System.out.println(i + " " + n);
        }

        // Instructions de branchement
        // break
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            if (i == 3) {
                break; // break => termine la boucle
            }
        }

        // continue
        for (int i = 0; i < 10; i++) {
            if (i == 3) {
                continue; // continue => on passe à l'itération suivante
            }
            System.out.println(i);
        }

        // LABEL
        EXIT_LOOP: for (int i = 0; i < 5; i++) {
            for (int k = 0; k < 10; k++) {
                System.out.println(i + " " + k);
                if (i == 2) {
                    break EXIT_LOOP; // se branche sur le label EXIT_LOOP et quitte les 2 boucles imbriquées
                }
            }
        }

        // Exercice: Table de multiplication
        // Faire un programme qui affiche la table de multiplication pour un nombre
        // entre 1 et 9
        //
        // 1 X 4 = 4
        // 2 X 4 = 8
        // …
        // 9 x 4 = 36
        //
        // Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on
        // redemande une nouvelle valeur

        for (;;) {
            int val = sc.nextInt();
            if (val < 1 || val > 9) {
                break;
            }
            for (int i = 1; i < 10; i++) {
                System.out.println(i + " x " + val + " = " + (i * val));
            }
        }

        // Exercice: Quadrillage
        // un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne

        // ex: pour 2 3
        // [ ][ ]
        // [ ][ ]
        // [ ][ ]
        System.out.println("Entrer le nombre de colonne");
        int col = sc.nextInt();
        System.out.println("Entrer le nombre de ligne");
        int row = sc.nextInt();
        for (int r = 0; r < row; r++) {
            for (int c = 0; c < col; c++) {
                System.out.print("[ ]");
            }
            System.out.print("\n");
        }
        
        sc.close();
    }

}
