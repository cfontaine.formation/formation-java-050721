import java.util.Scanner;

public class Tableaux {

    public static void main(String[] args) {
        
        // Déclaration d'un tableau à une dimension
//        double [] tab=null;   // On déclare une référence vers le tableau
//        tab=new double[3];    // On crée l'objet tableau de double 
      
        double[] tab = new double[3];
        // ou double[] tab = new double[3];
        
        // par défaut les éléments du tableau sont initialisés:
        // entier -> 0
        // double -> 0.0
        // char -> '\u0000'
        // boolean -> false
        // référence -> null

        tab[0] = 1.2;
        System.out.println(tab[0]); // affichage du premier élément du tableau

        // Parcourir un tableau avec un for
        for (int i = 0; i < tab.length; i++) {
            System.out.println(tab[i]);
        }

        // Parcourir un tableau complétement "foreach"
        for (double elm : tab) {
            System.out.println(elm);
        }

        // Déclaration et initialisation du tableau
        String[] tabStr = { "Azerty", "Bonjour", "HelloWorld" };
        for (String str : tabStr) {
            System.out.println(str);
        }

        // Indice en dehors du tableau
        // System.out.println(tabStr[10]); // => exception

        // Afficher la taille du tableau
        System.out.println("Nb élément= " + tabStr.length);

        Scanner sc = new Scanner(System.in);
        System.out.println("Taille du tableau");
        int size = sc.nextInt();
        int t[] = new int[size];
        for (int i : t) {
            System.out.println(i);
        }

        System.out.print("Saisir la taille du tableau "); // 2
        int s = sc.nextInt();
        int[] ti = new int[s];
        for (int i = 0; i < ti.length; i++) {
            System.out.print("ti[" + i + "]=");
            ti[i] = sc.nextInt();
        }

        // int ti[] = { -7, 4, 8, 0, -3 }; // 1
        int maximum = ti[0];// Integer.MIN_VALUE;
        double somme = 0.0;
        for (int elm : ti) {
            if (elm > maximum) {
                maximum = elm;
            }
            somme += elm;
        }

        System.out.println("maximum=" + maximum + " moyenne=" + somme / ti.length);

        // Tableau à 2 dimensions
        char[][] tabChr = new char[3][2];
        // ou char tabChr [][]=new char[3][2];
        // ou char [] tabChr []=new char[3][2];

        // accèder à un élément
        tabChr[1][0] = 'a';

        // Nombre d'élément sur la première dimension => nombre de ligne
        System.out.println(tabChr.length);

        // Nombre de colonne de la première ligne
        System.out.println(tabChr[0].length);

        for (int i = 0; i < tabChr.length; i++) {
            for (int j = 0; j < tabChr[i].length; j++) {
                System.out.print(" [ " + tabChr[i][j] + " ] ");
            }
            System.out.println("");
        }

        for (char[] ligne : tabChr) {
            for (char elm : ligne) {
                System.out.print(" [ " + elm + " ] ");
            }
            System.out.println("");
        }

        // Tableau à 3 dimensions
        boolean[][][] tab3D = new boolean[3][4][2];
        tab3D[0][3][1] = true;

        for (boolean tb[][] : tab3D) {
            for (boolean tc[] : tb) {
                for (boolean b : tc) {
                    System.out.println(b);
                }
            }
        }

        // Déclaration et initialisation d'un tableau en 2D
        int[][] tab2D = { { 1, 2 }, { 7, 5 }, { 5, 9 } };
        for (int[] ligne : tab2D) {
            for (int elm : ligne) {
                System.out.print(" [ " + elm + " ] ");
            }
            System.out.println("");
        }

        // Déclaration d'un tableau en escalier => chaque ligne a un nombre d'élement
        // différent
        double[][] tabEsc = new double[3][];
        tabEsc[0] = new double[3];
        tabEsc[1] = new double[2];
        tabEsc[2] = new double[4];

        for (double[] ligne : tabEsc) {
            for (double elm : ligne) {
                System.out.print(" [ " + elm + " ] ");
            }
            System.out.println("");
        }

        int[][] tabEsc2 = { { 1, 3 }, { 1, 3, 6, 9 }, { 5 }, { 2, 6, 7 } };
        for (int i = 0; i < tabEsc2.length; i++) {
            for (int j = 0; j < tabEsc2[i].length; j++) {
                System.out.print(" [ " + tabEsc2[i][j] + " ] ");
            }
            System.out.println("");
        }
        sc.close();
    }

}
