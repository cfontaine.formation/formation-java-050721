import java.time.LocalDate;

public class DataType {

    // Méthode main point d'entré du programme
    public static void main(String[] args) {
        
        // Déclaration d'une variable
        int i;
        // System.out.println(i); // En java, on ne peut pas utiliser une variable non
        // initlisée

        // Initialisation d'une variable
        i = 42;
        System.out.println(i);

        // Déclaration multiple
        double hauteur, largeur;
        hauteur = 1.0;
        largeur = 3.5;
        System.out.println(hauteur + " " + largeur);

        // Déclaration et initialisation d'une variable
        double d = 10.7;
        boolean test = false, isOk;
        System.out.println(d + " " + test);

        // Par défaut une valeur littérale entière est de type int
        long l = 123456789689L; // L -> littéral de type long
        System.out.println(l);

        // Par défaut une valeur littérale réel est de type double
        float f = 1.2f; // f -> littéral de type float
        System.out.println(f);

        // Littéraux changement de base
        int dec = 103; // décimal (base 10) par défaut
        int hex = 0xF30a; // 0x -> hexadécimal
        int oct = 045; // 0 -> Octal
        int bin = 0b10010110; // 0b -> binaire
        System.out.println(dec + " " + hex + " " + oct + " " + bin);

        // Séparateur _
        // Pas de séparateur _ en début, en fin , avant et après la virgule
        int mille = 1_000;
        double sep = 1_000_456.50;
        System.out.println(mille + " " + sep);

        // Littéral caractère
        char ch = 'a';
        char chrUtf8 = '\u0061';
        System.out.println(ch + " " + chrUtf8);

        // Littéral boolean
        boolean b = true; // false
        System.out.println(b);

        // Littéral réel
        double d1 = 10.56;
        double d2 = 1.23e3;
        double d3 = .56;
        System.out.println(d1 + " " + d2 + " " + d3);

        // Transtypage implicite (pas de perte de donnée)
        // Type inférieur vers un type supérieur
        short s = 12;
        int tii = s;
        // Entier vers un réel
        double tid = tii;
        System.out.println(s + " " + tii + " " + tid);

        // Transtypage explicite : cast = (nouveauType)
        double ted = 234.5;
        int tei = (int) ted;
        // Type supérieur vers un type inférieur
        byte bei = (byte) tei;
        System.out.println(ted + " " + tei + " " + bei);

        // Transtypage explicite, dépassement de capacité
        int to = 300;        // 00000000 00000000 00000001 00101100 300
        byte tb = (byte) to; //                            00101100 44
        System.out.println(to + " " + tb);

        // Type référence
        String str1 = new String("Hello"); // ou "Hello";
        String str2 = null; // str2 ne référence aucun objet
        System.out.println(str1 + " " + str2);
        str2 = str1; // str1 et str2 font références au même objet
        System.out.println(str1 + " " + str2);
        str1 = null;
        System.out.println(str1 + " " + str2);
        str2 = null;
        // str1 et str2 sont égales à null
        // Il n'y a plus de référence sur l'objet
        // Il éligible à la destruction par le garbage collector

        // LocalDate date = null;
        // date=str1;

        // Wrapper
        Integer iWarp = new Integer(12);
        System.out.println(iWarp);

        // Convertion String en type primitif
        double dC = Double.parseDouble("12.3");
        System.out.println(dC);

        // Convertion String vers Wrapper
        Double dcWarp = Double.valueOf("45.6");
        System.out.println(dcWarp);

        // Conversion Double vers long
        long ll = dcWarp.longValue();
        System.out.println(ll);
        
        // Affichage d'un nombre au format binaire, hexadecimal, octal
        String strBin = Integer.toBinaryString(12);
        String strHex = Integer.toHexString(12);
        String strOct = Integer.toOctalString(12);
        System.out.println(strBin + " " + strHex + " " + strOct);
    }

}
