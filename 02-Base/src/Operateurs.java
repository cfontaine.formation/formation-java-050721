import java.util.Scanner;

public class Operateurs {

    public static void main(String[] args) {
        // Opérateur arithméthique
        int a = 1;
        int b = 3;
        int c = a + b;
        System.out.println(c);
        System.out.println(3 % 2); // % modulo => reste de la division entière

        // Incrementation/décrémentation
        // Pré-incrémentation
        int inc = 0;
        int res = ++inc; // Incrémentation de inc et affectation de res avec la valeur de inc
        System.out.println(inc + " " + res); // inc=1 res=1

        // Post-incrémentation
        inc = 0;
        res = inc++; // Affectation de res avec la valeur de inc et incréméntation de inc
        System.out.println(inc + " " + res); // res=0 inc=1

        // Affectation composée
        a = 12;
        res += a; // res=res+a
        System.out.println(res);

        // Opérateur de comparaison
        // Une comparaison a pour résultat un booléen
        a = 23;
        b = 6;
        boolean tst1 = a == b; // faux
        System.out.println(tst1);
        boolean tst2 = a != b; // vrai
        System.out.println(tst2);

        // Opérateur logique
        // ! => Opérateur non
        boolean tst5 = !tst2; // faux
        System.out.println(tst5);

        // Opérateur court-circuit && et ||
        // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
        boolean tst3 = a > 100 && b == 6; // faux
        System.out.println(tst3);
        
        // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
        boolean tst4 = a < 23 || b != 6; // true
        System.out.println(tst4);
        
        // Opérateur Binaire
        byte bin = 0b100110;
        System.out.println(Integer.toBinaryString(~bin)); // complément => 11001
        System.out.println(Integer.toBinaryString(bin & 0b101)); // et => 100
        System.out.println(Integer.toBinaryString(bin | 0b101)); // ou => 100111
        System.out.println(Integer.toBinaryString(bin ^ 0b101)); // ou exclusive => 100011

        // Opérateur de décalage
        System.out.println(Integer.toBinaryString(bin << 2)); // décallage à gauche de 2 bits => 10011000
        System.out.println(Integer.toBinaryString(bin >> 1)); // décallage à droite de 1 bits => 10011, Insertion à
                                                              // droite bit de signe
        System.out.println(Integer.toBinaryString(bin >>> 1)); // décallage à droite de 1 bits => 10011, Insertion à
                                                               // droite 0
        // Promotion numérique
        // 1=> Le type le + petit est promu vers le + grand type des deux
        float prf = 1.23F;
        double prd = 34.7;
        double r1 = prf + prd; // 4=> Après une promotion le résultat aura le même type
        System.out.println(r1);

        // 2=> La valeur entière est promue en virgule flottante
        int pri = 11;
        int r21 = pri / 2; // 5
        double r22 = pri / 2.0; // 5.5
        System.out.println(r21 + " " + r22);

        // 3=> byte, short, char sont promus en int
        short s1 = 12;
        short s2 = 30;
        // short r3=s1+s2;
        int r3 = s1 + s2;
        System.out.println(r3);

        // Saisie dans la console => Scanner
        Scanner scan = new Scanner(System.in);
        int si = scan.nextInt();
        System.out.println(si);

        // Exercice Somme
        // Saisir 2 chiffres et afficher le résultat dans la console
        // sous la forme 1 + 3 = 4
        System.out.print("Entrer deux nombres entier");
        int v1 = scan.nextInt();
        int v2 = scan.nextInt();
        int add = v1 + v2;
        System.out.println(v1 + " + " + v2 + " = " + add);

        // Exercice Moyenne
        // Saisir 2 chiffres et afficher la moyenne des 2 nombres dans la console
        System.out.print("Entrer deux nombres entier");
        int va = scan.nextInt();
        int vb = scan.nextInt();
        double moy = (va + vb) / 2.0; // ((double)(va+vb))/2
        System.out.println(moy);

        scan.close();
    }

}
