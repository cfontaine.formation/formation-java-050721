import java.util.Scanner;

public class Methodes {

    public static void main(String[] args) {
        // Appel de la méthode
        double r = somme(1.4, 5.8);
        System.out.println(r);
        System.out.println(somme(5.0, 7.0));
        afficher(2);

        Scanner scan = new Scanner(System.in);
        // Exercice: appel de la méthode maximum
        double d1 = scan.nextDouble();
        double d2 = scan.nextDouble();
        System.out.println("Maximum=" + maximum(d1, d2));
        System.out.println(paire(3));
        System.out.println(paire(4));

        // test des arguments par valeurs (paramètre de type primitif)
        int test = 23;
        testParamVal(test);
        System.out.println(test);

        // test des arguments par valeurs (paramètre de type référence)
        StringBuilder b = new StringBuilder("Value");
        testParamRef(b);
        System.out.println(b);

        // test des arguments par valeurs (Modification de l'état de l'objet)
        testParamRefObj(b);
        System.out.println(b);

        // Nombre d'arguments variable
        double m = moyenne();
        System.out.println(m);
        System.out.println(moyenne(2.6));
        System.out.println(moyenne(2.6, 5.8, 4.7));

        double tv[] = { 4.8, 5.0, 6.0 };
        System.out.println(moyenne(tv));

        // Surcharge de méthode
        System.out.println(multiplier(3, 2));
        System.out.println(multiplier(3.5, 2.7));
        System.out.println(multiplier(2, 5.8));

        System.out.println(multiplier(2.3, 5));

        // Affichage des paramètres passés à la méthode main
        for (String a : args) {
            System.out.println(a);
        }

        // Méthode Récursive
        int res = factorial(3);
        System.out.println(res);

        // Exercice tableau
        menu(scan);
        scan.close();
    }

    // Déclaration d'une méthode
    static double somme(double d1, double d2) {
        return d1 + d2; // return => Interrompt l'exécution de la méthode
                        // => Retourne la valeur (expression à droite)
    }

    static void afficher(int v) {
        System.out.println("valeur=" + v);
        // avec void => return; ou return peut être omis
    }

    // Exercice Maximum
    // Écrire une fonction maximum qui prends en paramètre 2 nombres et retourne
    // le maximum
    // Saisir 2 nombres et afficher le maximum entre ces 2 nombres
    static double maximum(double v1, double v2) {
//       if(v1>v2) {
//           return v1;
//       }
//       else {
//           return v2;
//       }
        return v1 > v2 ? v1 : v2;
    }

    static boolean paire(int val) {
//       if(val%2==0)  {
//           return true;
//       }
//       else {
//           return false;
//       }
        return val % 2 == 0;
    }

    // test des arguments par valeurs (paramètre de type primitif)
    static void testParamVal(int a) {
        System.out.println(a);
        a = 10; // la modification du paramètre a n'a pas de répercution en dehors de la méthode
        System.out.println(a);
    }

    // test des arguments par valeurs (paramètre de type référence)
    static void testParamRef(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie la valeur de la référence sb, il n'a pas répercution en dehors de la méthode
        sb = new StringBuilder("Other Value");
        System.out.println(sb);
    }

    // test des arguments par valeurs (Modification de l'état de l'objet)
    static void testParamRefObj(StringBuilder sb) {
        System.out.println(sb);
        // si on modifie la valeur de la référence str, il n'a pas répercution en dehors de la méthode
        sb.append("et Other Value");
        System.out.println(sb);
    }

    // Nombre d'arguments variable
    static double moyenne(double... values) {
        // dans la méthode values est considérée comme un tableau
        if (values.length == 0) {
            return 0;
        } else {
            double somme = 0.0;
            for (double e : values) {
                somme += e;
            }
            return somme / values.length;
        }
    }

    // Surcharge de méthode
    // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même
    // nom, mais leurs signatures doient être différentes
    // La signature d'une méthode correspond aux types et nombre de paramètres
    // Le type de retour ne fait pas partie de la signature
    static int multiplier(int a, int b) {
        System.out.println("2 entiers");
        return a * b;
    }

    static double multiplier(double v1, double v2) {
        System.out.println("2 double");
        return v1 * v2;
    }

    static double multiplier(int v1, double v2) {
        System.out.println("un entier et un double");
        return v1 * v2;
    }

    // Méthode récursive
    static int factorial(int n) { // factoriel= 1* 2* … n
        if (n <= 1) { // condition de sortie
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }

//  Tableau
//  - Ecrire un méthode qui affiche un tableau d’entier passé en paramètre
//  - Ecrire une méthode qui permet de saisir :
//        - La taille du tableau
//        - Les éléments du tableau
//  - Ecrire une méthode qui calcule le maximum et la moyenne 
//  - Faire un menu qui permet de lancer ces méthodes:Faire un menu qui permet de lancer ces méthodes
    static void afficherTab(int[] v) {
        System.out.print("[ ");
        for (int i : v) {
            System.out.print(i + " ");
        }
        System.out.println("]");
    }

    static int[] saisirTab(Scanner sc) {
        System.out.print("Entrer la taille du tableau ");
        int size = sc.nextInt();
        int[] tmp = new int[size];
        for (int i = 0; i < tmp.length; i++) {
            System.out.print("t[" + i + "]=");
            tmp[i] = sc.nextInt();
        }
        return tmp;
    }

    static int maximumTab(int[] t) {
        int max = t[0];
        for (int v : t) {
            if (v > max) {
                max = v;
            }
        }
        return max;
    }

    static double moyenneTab(int[] t) {
        double somme = 0.0;
        for (int v : t) {
            somme += v;
        }
        return somme / t.length;
    }

    static void afficherMenu() {
        System.out.println("1- Saisie du tableau");
        System.out.println("2- Afficher le tableau");
        System.out.println("3- Calculer  le maximum et la moyenne");
        System.out.println("4- Quitter");
    }

    static void menu(Scanner sc) {
        afficherMenu();
        int[] tab = null;
        int choix;
        do {
            choix = sc.nextInt();
            switch (choix) {
            case 1:
                tab = saisirTab(sc);
                break;
            case 2:
                if (tab != null) {
                    afficherTab(tab);
                } else {
                    System.out.println("Il n'y a pas de tableau saisie");
                }
                break;
            case 3:
                if (tab != null) {
                    System.out.println("Maximum= " + maximumTab(tab) + " Moyenne= " + moyenneTab(tab));
                } else {
                    System.out.println("Il n'y a pas de tableau saisie");
                }
                break;
            case 4:
                System.out.println("Au revoir");
                break;
            default:
                System.out.println("Erreur choix");
                afficherMenu();
            }
        } while (choix != 4);

    }
}
