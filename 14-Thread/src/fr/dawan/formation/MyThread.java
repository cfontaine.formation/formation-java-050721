package fr.dawan.formation;

public class MyThread extends Thread {

    private int tsl;

    public MyThread(int tsl) {
        this.tsl = tsl;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getName() + " " + i);
            try {
                sleep(tsl); // Va mettre en pause l'execution du Thread pendant tsl minutes
            } catch (InterruptedException e) { // On ne peut capturer une InterruptedException que sur une méthode sleep
                e.printStackTrace();
                break;
            }
        }
    }

}
