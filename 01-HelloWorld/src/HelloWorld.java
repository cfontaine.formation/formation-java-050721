/**
 * 
 * @author Jehann
 *
 */
public class HelloWorld {
/*
 * Commentaire 
 * sur plusieurs
 * lignes
 */
    
    // Commentaire sur une ligne
    
    /**
     * Point d'entrée du programme
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Hello World!!!");
    }

}
