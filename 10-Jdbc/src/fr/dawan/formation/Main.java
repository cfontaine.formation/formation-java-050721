package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import fr.dawan.formation.beans.Contact;
import fr.dawan.formation.dao.ContactDao2;

public class Main {

    public static void main(String[] args) {
        ContactDao2 dao = new ContactDao2();
        Contact c = new Contact("Maurice", "Dupont", "md@dawan.com", LocalDate.of(1978, 12, 2));
        dao.saveOrUpdate(c, false);
        System.out.println(c);
        Contact c2 = dao.findById(1, false);
        System.out.println(c2);
        if (c2 != null) {
            dao.remove(c2, false);
        }
        List<Contact> lst = dao.findAll(true);
        for (Contact co : lst) {
            System.out.println(co);
        }

    }

}
