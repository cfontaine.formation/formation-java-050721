package fr.dawan.formation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import fr.dawan.formation.beans.Contact;

public class TestJdbc {

    public static void main(String[] args) {
        Contact c1 = new Contact("Alan", "Smithee", "alan@dawan.com", LocalDate.of(1960, 10, 1));
        System.out.println(c1);
        insertContact(c1);
        System.out.println(c1);
        readContacts();
    }

    public static void insertContact(Contact c) {
        Connection cnx = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation", "root", "dawan");
//            Statement stm=cnx.createStatement();
//            String requete="INSERT INTO contacts(prenom,nom,email,date_naissance) VALUE('"+ 
//            c.getPrenom() + "','"+c.getNom()+ "','"+c.getEmail()+"','"+c.getDateNaissance() +"')";
//            System.out.println(requete);
//            System.out.println(stm.executeUpdate(requete));
            cnx.setAutoCommit(false);

            PreparedStatement pstm = cnx.prepareStatement(
                    "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUE(?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            pstm.setString(1, c.getPrenom());
            pstm.setString(2, c.getNom());
            pstm.setString(3, c.getEmail());
            pstm.setDate(4, Date.valueOf(c.getDateNaissance()));
            System.out.println(pstm.executeUpdate());
            cnx.commit();
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                c.setId(rs.getLong(1));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            if (cnx != null) {
                try {
                    cnx.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public static void readContacts() {
        Connection cnx = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/formation", "root", "dawan");

            Statement stm = cnx.createStatement();
            String req = "SELECT * FROM contacts ";
            ResultSet rs = stm.executeQuery(req);
            while (rs.next()) {
                System.out.println(rs.getLong("id"));
                System.out.println(rs.getString("prenom"));
                System.out.println(rs.getString("nom"));
                System.out.println(rs.getString("email"));
                System.out.println(rs.getDate("date_naissance"));
            }
            rs.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

    }

}
