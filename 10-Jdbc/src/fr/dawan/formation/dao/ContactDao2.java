package fr.dawan.formation.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.beans.Contact;

public class ContactDao2 extends AbstractDao<Contact> {

    @Override
    protected void remove(Long id, Connection cnx) throws SQLException {
        PreparedStatement ps = null;
        ps = cnx.prepareStatement("DELETE FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    protected Contact findById(Long id, Connection cnx) throws SQLException {
        Contact co = null;
        PreparedStatement ps = cnx.prepareStatement("SELECT * FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            co = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                    rs.getDate("date_naissance").toLocalDate());
            co.setId(id);
        }
        return co;
    }

    @Override
    protected List<Contact> findAll(Connection cnx) throws SQLException {
        List<Contact> lst = new ArrayList<>();
        PreparedStatement ps = cnx.prepareStatement("SELECT * FROM contacts");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Contact co = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                    rs.getDate("date_naissance").toLocalDate());
            co.setId(rs.getLong("id"));
            lst.add(co);
        }
        return lst;
    }

    @Override
    protected void insert(Contact e, Connection cnx) throws SQLException {
        PreparedStatement pstm = cnx.prepareStatement(
                "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUE(?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS);
        pstm.setString(1, e.getPrenom());
        pstm.setString(2, e.getNom());
        pstm.setString(3, e.getEmail());
        pstm.setDate(4, Date.valueOf(e.getDateNaissance()));
        pstm.executeUpdate();

        ResultSet rs = pstm.getGeneratedKeys();
        if (rs.next()) {
            e.setId(rs.getLong(1));
        }

    }

    @Override
    protected void update(Contact e, Connection cnx) throws SQLException {
        PreparedStatement pstm = cnx
                .prepareStatement("UPDATE contacts SET prenom=?,nom=?,email=?,date_naissance=? WHERE id=?");
        pstm.setString(1, e.getPrenom());
        pstm.setString(2, e.getNom());
        pstm.setString(3, e.getEmail());
        pstm.setDate(4, Date.valueOf(e.getDateNaissance()));
        pstm.setLong(5, e.getId());
        pstm.executeUpdate();

    }

}
