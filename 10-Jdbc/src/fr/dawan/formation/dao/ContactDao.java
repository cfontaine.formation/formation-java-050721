package fr.dawan.formation.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.beans.Contact;

public class ContactDao {

    private static Connection cnx;

    private static Properties dbProperties;

    public void saveOrUpdate(Contact c, boolean close) {
        if (c.getId() == 0) {
            insert(c, close);
        } else {
            update(c, close);
        }
    }

    public void remove(Contact c, boolean close) {
        remove(c.getId(), close);
    }

    public void remove(long id, boolean close) {
        Connection c = getConnection();
        PreparedStatement ps = null;
        try {
            ps = c.prepareStatement("DELETE FROM contacts WHERE id=?");
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
    }

    public Contact findById(long id, boolean close) {
        Contact co = null;
        Connection c = getConnection();
        try {
            PreparedStatement ps = c.prepareStatement("SELECT * FROM contacts WHERE id=?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                co = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                        rs.getDate("date_naissance").toLocalDate());
                co.setId(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
        return co;
    }

    public List<Contact> findAll(boolean close) {
        List<Contact> lst = new ArrayList<>();
        Connection c = getConnection();
        try {
            PreparedStatement ps = c.prepareStatement("SELECT * FROM contacts");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contact co = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                        rs.getDate("date_naissance").toLocalDate());
                co.setId(rs.getLong("id"));
                lst.add(co);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
        return lst;
    }

    private void insert(Contact co, boolean close) {
        Connection c = getConnection();
        try {
            PreparedStatement pstm = c.prepareStatement(
                    "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUE(?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            pstm.setString(1, co.getPrenom());
            pstm.setString(2, co.getNom());
            pstm.setString(3, co.getEmail());
            pstm.setDate(4, Date.valueOf(co.getDateNaissance()));
            pstm.executeUpdate();

            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                co.setId(rs.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
    }

    private void update(Contact co, boolean close) {
        Connection c = getConnection();
        try {
            PreparedStatement pstm = c
                    .prepareStatement("UPDATE contacts SET prenom=?,nom=?,email=?,date_naissance=? WHERE id=?");
            pstm.setString(1, co.getPrenom());
            pstm.setString(2, co.getNom());
            pstm.setString(3, co.getEmail());
            pstm.setDate(4, Date.valueOf(co.getDateNaissance()));
            pstm.setLong(5, co.getId());
            pstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
    }

    private static Connection getConnection() {
        if (cnx == null) {
            if (dbProperties == null) {
                try (FileInputStream fi = new FileInputStream("c:\\Formations\\TestIO\\db.properties")) {
                    dbProperties = new Properties();
                    dbProperties.load(fi);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                Class.forName(dbProperties.getProperty("driverdb"));
                cnx = DriverManager.getConnection(dbProperties.getProperty("urldb"), dbProperties.getProperty("userdb"),
                        dbProperties.getProperty("passworddb"));
            } catch (ClassNotFoundException e) {

                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cnx;
    }

    private static void closeConnection() {
        if (cnx != null) {
            try {
                cnx.close();
                cnx = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
