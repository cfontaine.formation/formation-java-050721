package fr.dawan.formation.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.beans.DbObject;

public abstract class AbstractDao<T extends DbObject> {

    private static Connection cnx;

    private static Properties dbProperties;

    public void saveOrUpdate(T e, boolean close) {
        Connection c = getConnection();
        try {
            if (e.getId() == 0) {
                insert(e, c);
            } else {
                update(e, c);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
    }

    public void remove(T e, boolean close) {
        remove(e.getId(), close);
    }

    public void remove(long id, boolean close) {
        Connection c = getConnection();
        try {
            remove(id, c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
    }

    public T findById(long id, boolean close) {
        T co = null;
        Connection c = getConnection();
        try {
            co = findById(id, c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
        return co;
    }

    public List<T> findAll(boolean close) {
        List<T> lst = new ArrayList<>();
        Connection c = getConnection();
        try {
            lst = findAll(c);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
        return lst;
    }

    protected abstract void remove(Long id, Connection cnx) throws SQLException;

    protected abstract T findById(Long id, Connection cnx) throws SQLException;

    protected abstract List<T> findAll(Connection cnx) throws SQLException;

    protected abstract void insert(T e, Connection cnx) throws SQLException;

    protected abstract void update(T e, Connection cnx) throws SQLException;

    protected static Connection getConnection() {
        if (cnx == null) {
            if (dbProperties == null) {
                try (FileInputStream fi = new FileInputStream("c:\\Formations\\TestIO\\db.properties")) {
                    dbProperties = new Properties();
                    dbProperties.load(fi);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                Class.forName(dbProperties.getProperty("driverdb"));
                cnx = DriverManager.getConnection(dbProperties.getProperty("urldb"), dbProperties.getProperty("userdb"),
                        dbProperties.getProperty("passworddb"));
            } catch (ClassNotFoundException e) {

                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cnx;
    }

    protected static void closeConnection() {
        if (cnx != null) {
            try {
                cnx.close();
                cnx = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
