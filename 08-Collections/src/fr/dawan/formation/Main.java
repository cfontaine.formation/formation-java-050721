package fr.dawan.formation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        // Avant java SE 5 => on stocker tous objets qui héritent de Object
        List lst1 = new ArrayList(); // List => interface , ArrayList =>objet "réel" qui implémente l'interface
        lst1.add("azerty"); // add => ajouter un objet à la collection
        lst1.add("Hello");
        lst1.add(123); // Autoboxing type primitf-> type envelope
        lst1.add(3.5);
        lst1.remove(1);
        if (lst1.get(0) instanceof String) { // get => récupérer un objet stocké dans la liste à l'indice 0
            String str = (String) lst1.get(0); // retourne des objects, il faut tester si l'objet correspont à la classe
                                               // et le caster
        }

        // Parcourir une collection avec Iterateur
        Iterator iter = lst1.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        // Java 5 => les types générique
        List<String> lst2 = new LinkedList<>(); // la liste ne peut plus contenir que des chaines de caractères
        lst2.add("Hello");
        lst2.add(0, "Bonjour"); // insertion à l'index 0 uniquement pour les listes
        lst2.add("azerty");
        lst2.add("asupprimerindex");
        lst2.add("asupprimer");
        // lst2.add(12);
        String str2 = lst2.get(0); // get => revoie l'élément placé à l'index
        System.out.println(lst2.size()); // size => le nombre d'élément de la collection
        System.out.println(lst2.set(2, "AZERTY")); // set => remplace l'élément à l'index par celui passé en paramètre

        lst2.remove("asupprimer"); // supprime le premier élément "asupprimer"
        lst2.remove(3); // supprimer l'élément à l'index 3
        boolean test = lst2.remove("existepas");
        System.out.println(test);
        // Parcourir une collection "foreach"
        for (String s : lst2) {
            System.out.println(s);
        }

        // Set => collection qui ne contient pas de doublons
        Set<Integer> st1 = new HashSet<>();
        System.out.println(st1.add(2));
        st1.add(4);
        System.out.println(st1.add(2)); // add retourne false 2 existe déjà dans la collection
        for (int i : st1) {
            System.out.println(i);
        }

        // Sorted Set => tous les objets sont automatiquement triés
        // Set trié => interface Comparator
        // Classe anonyme
        // SortedSet<Integer> st2=new TreeSet<>(new IntegerComparator());
        // st2.add(100);
        // st2.add(4);
        // st2.add(1000);
        // for (int i : st2) {
        //      System.out.println(i);
        // }

        // Set trié => interface Comparable
        SortedSet<Integer> st2 = new TreeSet<>(new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                return -o1.compareTo(o2);
            }
        });
        st2.add(100);
        st2.add(4);
        st2.add(1000);
        for (int i : st2) {
            System.out.println(i);
        }

        // Set trié => interface Comparable
        SortedSet<User> setUser = new TreeSet<>();
        setUser.add(new User("Marcel", "Dupont"));
        setUser.add(new User("John", "Doe"));
        setUser.add(new User("Jane", "Doe"));
        setUser.add(new User("Alan", "Smithee"));

        for (User u : setUser) {
            System.out.println(u);
        }

        // Queue => file d'attente
        Queue<Double> pileFifo = new LinkedList<>();
        pileFifo.offer(3.4);
        pileFifo.offer(4.9);
        pileFifo.offer(54.7);
        System.out.println(pileFifo.peek());
        System.out.println(pileFifo.peek());
        System.out.println(pileFifo.poll());
        System.out.println(pileFifo.peek());

        // Map => association Clé/Valeur 
        Map<String, User> m = new HashMap<>();
        m.put("fr59-001", new User("Marcel", "Dupont"));    // put => ajouter la clé "fr59-001" et la valeur associé (objet User)
        m.put("fr59-002", new User("John", "Doe"));
        m.put("fr59-003", new User("Alan", "Smithee"));
        m.put("fr59-004", new User("Jane", "Doe"));
        
        System.out.println(m.get("fr59-003"));  // Obtenir la valeur associé à la clé fr59-003
        System.out.println(m.containsKey("fr59-005"));   // false,la clé fr59-005 n'est pas présente dans la map
        System.out.println(m.containsValue(new User("Marcel", "Dupont")));  // true,la valeur est présente dans la map

        m.put("fr59-001", new User("Marcel", "Dupond"));    //si existe déjà, elle est écrasée par la nouvelle valeur associé à la clé 

        Set<String> keys = m.keySet();  // keySet => retourne toutes les clés de la map
        for (String s : keys) {
            System.out.println(s);
        }

        Collection<User> vals = m.values(); // values => retourne toutes les valeurs de la map
        for (User u : vals) {
            System.out.println(u);
        }

        // Entry => objet qui contient une clé et la valeur associée 
        Set<Entry<String, User>> ent = m.entrySet();
        for (Entry<String, User> e : ent) {
            System.out.println(e.getKey());
            System.out.println(e.getValue());
        }

        // Classe Collections => Classe utilitaires pour les collections
        System.out.println(Collections.min(lst2));  // min=> l'élément minimum de la collection 
        Collections.sort(lst2); // sort => pour trier une liste
        for (String s : lst2) {
            System.out.println(s);
        }

        // Classe Arrays  => Classe utilitaires pour les tableaux
        String[] tabStr = new String[5];
        Arrays.fill(tabStr, "Bonjour");     // => initialiser un tableau avec une valeur

        System.out.println(Arrays.toString(tabStr));    // toString =>  afficher un tableau
        int tab1[] = { 1, 5, 7, 3, 8 };
        int tab2[] = { 1, 5, 7, 3, 8 };
        System.out.println(Arrays.equals(tab1, tab2));  // equals => comparaison de deux tableaux
        Arrays.sort(tab2);                              // sort => tri d'un tableau
        System.out.println(Arrays.toString(tab2));

        System.out.println(Arrays.binarySearch(tab2, 5));   // binarySearch => recherche d'un élément dans un tableau trié
        int index = Arrays.binarySearch(tab2, 6);
        System.out.println((index + 1) * -1);
    }

}
