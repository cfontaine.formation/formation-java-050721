package fr.dawan.formation;

import java.util.Objects;

// Pour le tri avec sortedset la classe User implémente l'interface Comparable
public class User implements Comparable<User> {

    private String prenom;
    private String nom;

    public User(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        return Objects.equals(nom, other.nom) && Objects.equals(prenom, other.prenom);
    }

    @Override
    public String toString() {
        return "User [prenom=" + prenom + ", nom=" + nom + "]";
    }

    @Override
    public int compareTo(User o) {
        if (nom.compareToIgnoreCase(o.nom) > 0) {
            return 1;
        } else if (nom.compareToIgnoreCase(o.nom) != 0) {
            return -1;
        } else if (prenom.compareTo(o.prenom) > 0) {
            return 1;
        } else if (prenom.compareTo(o.prenom) != 0) {
            return -1;
        } else {
            return 0;
        }
    }

}
