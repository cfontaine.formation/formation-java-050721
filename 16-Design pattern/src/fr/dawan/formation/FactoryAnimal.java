package fr.dawan.formation;

public class FactoryAnimal {

    public static Animal createAnimal(AnimalEnum typeAnimal) {
        switch (typeAnimal) {
        case CHIEN:
            return new Chien();
        case CHAT:
            return new Chat();
        case CANARD:
            return new Canard();
        }
        return null;
    }

}
