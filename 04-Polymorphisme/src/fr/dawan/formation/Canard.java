package fr.dawan.formation;

public class Canard extends Animal implements PeutMarcher, PeutVoler { // On peut implémenter plusieurs d'interface

    public Canard(double poid, int age) {
        super(poid, age);
    }

    @Override
    public void decoller() {
        System.out.println("Le canard décolle");

    }

    @Override
    public void atterir() {
        System.out.println("Le canard attérit");

    }

    @Override
    public void marcher() {
        System.out.println("Le canard marche");

    }

    @Override
    public void courrir() {
        System.out.println("Le canard court");

    }

    @Override
    public void emettreSon() {
        System.out.println("Coin coin");

    }

}
