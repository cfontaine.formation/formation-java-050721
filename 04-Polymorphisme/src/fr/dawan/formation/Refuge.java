package fr.dawan.formation;

public class Refuge {
    
    private Animal[] place=new Animal[20];
    private int placeOcup;
    
    public void ajouter(Animal a) {
        if(placeOcup<place.length) {
            place[placeOcup]=a;
            placeOcup++;
        }
    }
    
    public void ecouter() {
        for(int i=0;i<placeOcup;i++) {
            place[i].emettreSon();
        }
    }

}
