package fr.dawan.formation;

// Pour pouvoir cloner un  objet avec la méthode clone de Object, il faut redéfinir la méthode clone
// et il faut que la classe impléménte l'interface Cloneable (interface marqueur qui ne contient aucune méthode)
public class Chat extends Animal implements PeutMarcher, Cloneable {

    public Chat(double poid, int age) {
        super(poid, age);
    }

    @Override
    public void emettreSon() {
        System.out.println("Le chat miaule");
    }

    @Override
    public void marcher() {
        System.out.println("Le chat marche");

    }

    @Override
    public void courrir() {         
        System.out.println("Le chart court");

    }

    // Redéfinition de la méthode clone (on augmente la visibilité de la méthode protected -> public)
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
