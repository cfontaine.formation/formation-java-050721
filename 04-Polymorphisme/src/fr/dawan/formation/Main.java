package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
//        Animal a1= new Animal(3000,7);    // Impossible la classe est abstraite
//        a1.emettreSon();

        Chien ch1 = new Chien(4000, 4, "Laika");
        ch1.emettreSon();

        Animal a2 = new Chien(2000, 9, "Idefix");   // On peut créer une instance de Chien (classe fille) qui aura une référence Animal (classe mère)
                                                    // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux méthodes propre au chien (nom,...)
        a2.emettreSon();                            // C'est la méthode emettreSon de Chien qui sera appelée                    
        System.out.println(a2.getAge());
        if (a2 instanceof Chien) {                  // test si a2 est de "type" Chien
            Chien ch2 = (Chien) a2;                 // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast
            System.out.println(ch2.getNom());       // avec la référence c2 (de type Chien), on a bien accès à toutes les méthodes de la classe Chien
        }

        Refuge r = new Refuge();
        r.ajouter(ch1);
        r.ajouter(a2);
        r.ajouter(new Chat(3000, 5));
        r.ajouter(new Canard(2000, 2));
        r.ecouter();

        PeutMarcher pm = new Chien(2000, 9, "Rantaplan");   // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
        pm.courrir();                                       // L'objet chien n'est vu que comme un interface PeutMarcher, on ne peut utiliser que les méthode de PeutMarcher (marcher, courrir)

        // Object
        // toString
        System.out.println(ch1);
        
        // equals
        Chat c1 = new Chat(3000, 5);
        Chat c2 = new Chat(3000, 5);

        System.out.println(c1 == c2);       // false, on compare les références (c1 et c2 sont 2 objets, leurs références sont différente) 
        System.out.println(c1.equals(c2));  // true, on compare le contenu des 2 objets, si dans la classe Chien equals est redéfinie

        // clone
        try {
            Chat c3 = (Chat) c2.clone();
            System.out.println(c3);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        // Enumération
        Direction dir = Direction.NORD;
        System.out.println(dir);
        
        // name ou toString : Enumeration -> String
        String str = dir.name();
        System.out.println(str);
        
        // valueOf : String -> Enumeration
        Direction dir2 = Direction.valueOf("EST");
        System.out.println(dir2);
        
        // ordinal : index de la valeur selon l'ordre de déclaration
        System.out.println(dir2.ordinal());
        
        // values() : retourne un tableau de toutes les valeurs énumérées disponibles
        Direction[] tab = Direction.values();
        for (Direction d : tab) {
            System.out.println(d);
        }
    }

}
