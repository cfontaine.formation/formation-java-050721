package fr.dawan.formation;

public interface PeutVoler {

    void decoller();

    void atterir();

}
