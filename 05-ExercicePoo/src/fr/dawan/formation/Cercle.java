package fr.dawan.formation;

public class Cercle extends Forme {

    private double rayon;

    public Cercle(double rayon, Couleur couleur) {
        super(couleur);
        this.rayon = rayon;
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    @Override
    public double calculSurface() {
        return Math.pow(rayon, 2.0) * Math.PI;
    }

    @Override
    public String toString() {
        return "Cercle [rayon=" + rayon + ", toString()=" + super.toString() + "]";
    }

}
