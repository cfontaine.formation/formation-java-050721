package fr.dawan.formation;

public class Application {

    public static void main(String[] args) {
        Terrain terrain=new Terrain();
        
        terrain.ajouter(new Rectangle(2.0,2.0,Couleur.BLEU));
        terrain.ajouter(new Rectangle(2.0,2.0,Couleur.BLEU));
        terrain.ajouter(new Rectangle(2.0,2.0,Couleur.BLEU));
        
        terrain.ajouter(new Cercle(1.0,Couleur.ROUGE));
        terrain.ajouter(new Cercle(1.0,Couleur.ROUGE));
        
        terrain.ajouter(new TriangleRectangle(1.0,1.0,Couleur.ORANGE));
        terrain.ajouter(new TriangleRectangle(1.0,1.0,Couleur.ORANGE));
        
        terrain.ajouter(new TriangleRectangle(1.0,1.0,Couleur.VERT));
        terrain.ajouter(new Rectangle(1.0,1.0,Couleur.VERT));
        terrain.ajouter(new Cercle(1.0,Couleur.VERT));
        
        
        System.out.println("Surface bleu:\t" + terrain.surfaceTotal(Couleur.BLEU));
        System.out.println("Surface rouge:\t" + terrain.surfaceTotal(Couleur.ROUGE));
        System.out.println("Surface orange:\t" + terrain.surfaceTotal(Couleur.ORANGE));
        System.out.println("Surface verte:\t" + terrain.surfaceTotal(Couleur.VERT));
        System.out.println("____________________");
        System.out.println("Surface total:\t"+ terrain.surfaceTotal());
    }

}
