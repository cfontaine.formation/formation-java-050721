package fr.dawan.formation;

public class Terrain {

    private Forme[] formes;
    private int nbForme;

    public Terrain() {
        formes = new Forme[10];
    }

    public Terrain(int size) {
        formes = new Forme[size];
    }

    public void ajouter(Forme forme) {
        if (nbForme < formes.length) {
            formes[nbForme] = forme;
            nbForme++;
        }
    }

    public double surfaceTotal() {
        double surface = 0.0;
        for (int i = 0; i < nbForme; i++) {
            surface += formes[i].calculSurface();
        }
        return surface;
    }

    public double surfaceTotal(Couleur couleur) {
        double surface = 0.0;
        for (int i = 0; i < nbForme; i++) {
            if (formes[i].getCouleur() == couleur) {
                surface += formes[i].calculSurface();
            }
        }
        return surface;
    }

}
